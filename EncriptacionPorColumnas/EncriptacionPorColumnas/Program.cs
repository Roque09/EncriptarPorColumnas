﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncriptacionPorColumnas
{
    class Program
    {
        static int  option = 0;
        static int methodEncrypt = 0;
        static void Main(string[] args)
        {
            int menu;
            do
            {
                DisplayMenu();

                if (option == 1)
                {

                    Console.Write("Ingresa la cadena a encriptar: ");
                    string mcla = Console.ReadLine();
                    int key;
                    string strKey;


                    switch (methodEncrypt)
                    {
                        case 1:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            key = int.Parse(Console.ReadLine());
                            Console.WriteLine(Encriptar.ColumnasSimple(mcla, key));
                            break;

                        case 2:
                            Console.Write("Ingresa la llave: ");
                            strKey = Console.ReadLine();
                            Console.WriteLine(Encriptar.ColumnasSimpleKey(mcla, strKey));
                            break;

                        case 3:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            key = int.Parse(Console.ReadLine());
                            Console.WriteLine(Encriptar.ColumnasDobles(mcla, key));
                            break;

                        case 4:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            strKey = Console.ReadLine();
                            Console.WriteLine(Encriptar.ColumnasDoblesKey(mcla, strKey.Length, strKey));
                            break;

                        default:
                            break;
                    }
                }
                else if (option == 2)
                {
                    Console.WriteLine("Ingresa la cadena a desencriptar: ");
                    string mcla = Console.ReadLine();
                    int key;
                    string strKey;

                    switch (methodEncrypt)
                    {
                        case 1:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            key = int.Parse(Console.ReadLine());
                           Console.WriteLine( Encriptar.Desencriptar.ColumnasSimple(mcla, key));
                            break;

                        case 2:
                            Console.Write("Ingresa la llave:");
                            strKey = Console.ReadLine();
                            Console.WriteLine(Encriptar.Desencriptar.ColumnasSimpleKey(mcla, strKey));
                            break;

                        case 3:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            key = int.Parse(Console.ReadLine());
                            Console.WriteLine(Encriptar.Desencriptar.ColumnasDobles(mcla, key));
                            break;

                        case 4:
                            Console.Write("Ingresa el numero de columnas deseado: ");
                            strKey = Console.ReadLine();
                            Console.WriteLine(Encriptar.Desencriptar.ColumnasDobleKey(mcla, strKey));
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    //Console.WriteLine("Algo salio mal");
                    DisplayMenu();
                }
                aqui:
               // Console.Clear();
                Console.WriteLine("\n1. Menu");
                Console.WriteLine("2. Salir");
                menu = int.Parse(Console.ReadLine());

                if (menu != 2 && menu != 1)
                    goto aqui;
                else if (menu == 2)
                    Environment.Exit(0);

            } while (menu == 1);

        }

        private static void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("¿Que desas hacer?");
            Console.WriteLine(" 1. Encriptar \n 2. Desencriptar");
            option = int.Parse(Console.ReadLine());
            Console.WriteLine("Elija un Metodo:");
            Console.WriteLine(" 1. Columnas Simples \n 2. Columnas simples con llave \n 3. Columnas dobles \n 4. Columnas dobles con llave");
            methodEncrypt = int.Parse(Console.ReadLine());
        }
    }
}
