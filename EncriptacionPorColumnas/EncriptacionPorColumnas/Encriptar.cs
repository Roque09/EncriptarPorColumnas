﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncriptacionPorColumnas
{
   public static class Encriptar
    {
        public static string ColumnasSimple(string mcla, int key)
        {
            mcla = mcla.Replace(" ", "");

            int r = Rows(ref mcla, key);

            string[,] EncryptArray = new string[r, key];

            EncryptArray = MclaToArray(mcla, EncryptArray, r, key);

            return MessageEncrypt(EncryptArray, r, key); 
        }

        public static string ColumnasSimpleKey(string mcla, string key)
        {
            mcla = mcla.Replace(" ", "");
            int c = key.Length;
            int r = Rows(ref mcla, key.Length);

            string[,] EncryptArray = new string[r, c];

            EncryptArray = MclaToArray(mcla, EncryptArray, r, c);

            return MessageEncryptKey(EncryptArray, r, c,key);
        }

        public static string ColumnasDobles(string mcla, int key)
        {
            mcla = mcla.Replace(" ", "");

            int r = Rows(ref mcla, key);

            string[,] EncryptArray = new string[r, key];

            EncryptArray = MclaToArray(mcla, EncryptArray, r, key);

            string mclaFirst = MessageEncrypt(EncryptArray, r, key);

            EncryptArray = MclaToArray(mclaFirst, EncryptArray, r, key);

            return MessageEncrypt(EncryptArray, r, key);
        }

        public static string ColumnasDoblesKey(string mcla, int key,string strKey)
        {
            mcla = mcla.Replace(" ", "");

            int r = Rows(ref mcla, key);

            string[,] EncryptArray = new string[r, key];

            EncryptArray = MclaToArray(mcla, EncryptArray, r, key);

            string mclaFirst = MessageEncrypt(EncryptArray, r, key);

            EncryptArray = MclaToArray(mclaFirst, EncryptArray, r, key);

            return MessageEncryptKey(EncryptArray, r, key,strKey);
        }

        private static int Rows(ref string mcla, int key)
        {
           
            double k = (double)key;
            double l = (double)mcla.Length;

            double rows = l / key;
            int r = (int)Math.Ceiling(rows);

            return r;
        }

        private static string MessageEncrypt(string[,] encryptArray, int r, int c)
        {
            string encrypt = "";

            for (int x = 0; x < c; x++)
            {
                for (int y = 0; y < r; y++)
                {
                    encrypt += encryptArray[y,x];
                }
            }
            return encrypt.ToUpper();
        }

        private static string MessageEncryptKey(string[,] encryptArray, int r, int c,string key)
        {
            string encrypt = "";
            //No ordenado
            char[] k = key.ToCharArray();
            //ordenado
            char[] k2 = key.ToCharArray();
            Array.Sort(k2);
            // string keysort = new string( k2);

            for (int i = 0; i < key.Length; i++)
            {
                int columnSelect = 0;
                for (int x = 0; x < k.Count(); x++)
                {
                    if (k2[i] == k[x])
                    {
                        columnSelect = x;
                        x = k.Count() + 1;
                    }
                }
                for (int y = 0; y < r; y++)
                {
                    encrypt += encryptArray[y, columnSelect];
                }
            }
            

            return encrypt.ToUpper();
        }

        private static string[,] MclaToArray(string mcla, string[,]EncryptArray, int r, int c)
        {
            char[] mclaArray = mcla.ToArray();
            int count = 0;
            for (int x = 0; x < r; x++)
            {
                for (int y = 0; y < c; y++)
                {
                    if (mcla.Length > count)
                    {
                        EncryptArray[x, y] = mclaArray[count].ToString();
                    }
                    else
                        EncryptArray[x, y] = "X";
                       count++;
                }

             
            }

            return EncryptArray;
        }


        public static class Desencriptar
        {
            public static string ColumnasSimple(string mcif, int key)
            {
                mcif = mcif.Replace(" ", "");

                int r = Rows(ref mcif, key);

                string[,] EncryptArray = new string[key, r];

                EncryptArray = MclaToArray(mcif, EncryptArray, key, r);

                return MessageEncrypt(EncryptArray, key, r);
            }

            public static string ColumnasSimpleKey(string mcif, string  key)
            {
                mcif = mcif.Replace(" ", "");
                int c = key.Length;
                int r = Rows(ref mcif, c);

                string[,] EncryptArray = new string[r, c];

                EncryptArray = MclaToArrayDesEncrypt(mcif, EncryptArray, c, r);

                return MessageEncryptKeyDes(EncryptArray, c, r, key);
            }

            public static string ColumnasDobles(string mcif, int key)
            {
                mcif = mcif.Replace(" ", "");

                int r = Rows(ref mcif, key);

                string[,] EncryptArray = new string[r, key];

                EncryptArray = MclaToArrayDesEncrypt(mcif, EncryptArray,key,r);

                string m = MessageEncryptHorizontal(EncryptArray, r, key);

                EncryptArray = MclaToArrayDesEncrypt(m, EncryptArray, key,r);

                string m2 = MessageEncryptHorizontal(EncryptArray, r, key);

                return m2;
            }

            public static string ColumnasDobleKey(string mcif, string key)
            {
                mcif = mcif.Replace(" ", "");
                int c = key.Length;
                int r = Rows(ref mcif, c);

                string[,] EncryptArray = new string[r, c];

                EncryptArray = MclaToArrayDesEncrypt(mcif, EncryptArray, c, r);

                string m = MessageEncryptKeyDes(EncryptArray, c, r, key);

                EncryptArray = MclaToArrayDesEncrypt(m, EncryptArray, c, r);

                return MessageEncryptHorizontal(EncryptArray, r, c);
            }

            //Lee el arreglo horizontal
            private static string MessageEncryptHorizontal(string[,] encryptArray, int r, int c)
            {
                string encrypt = "";

                for (int x = 0; x < r; x++)
                {
                    for (int y = 0; y < c; y++)
                    {
                        encrypt += encryptArray[x,y];
                    }
                }
                return encrypt.ToUpper();
            }
         
            //Ordena columnas alfabeticamente  y lee Horizontal
            private static string MessageEncryptKeyDes(string[,] encryptArray, int c, int r, string key)
            {
                string[,] encrypt = new string[r,c];
                //No ordenado
                char[] k = key.ToCharArray();
                //ordenado
                char[] k2 = key.ToCharArray();
                Array.Sort(k2);
                // string keysort = new string( k2);

                for (int i = 0; i < key.Length; i++)
                {
                    int columnSelect = 0;
                    for (int x = 0; x < k.Count(); x++)
                    {
                        if (k2[x] == k[i])
                        {
                            columnSelect = x;
                            x = k.Count() + 1;
                        }
                    }
                    //reordeno las columnas alfabeticamente
                    for (int y = 0; y < r; y++)
                    {
                        encrypt[y,i] = encryptArray[y,columnSelect] ;
                    }
                }
                //string result = "";
                //for (int x = 0; x < r; x++)
                //{
                //    for (int y = 0; y < k.Count(); y++)
                //    {
                //        result += encrypt[x, y];
                //    }
                //}
                //lee horizontalmente el mensaje en el arreglo
                string result = MessageEncryptHorizontal(encrypt, r, c);
                return result;
            }

            private static string[,] MclaToArrayDesEncrypt(string mcif, string[,] encryptArray, int c, int r)
            {
                char[] mcifArray = mcif.ToArray();
                int count = 0;
                for (int x = 0; x < c; x++)
                {
                    for (int y = 0; y < r; y++)
                    {
                        if (mcif.Length > count)
                        {
                            encryptArray[y,x] = mcifArray[count].ToString();
                        }
                        else
                            encryptArray[x, y] = "X";
                        count++;
                    }


                }

                return encryptArray;
            }
        }
    }
 
}
